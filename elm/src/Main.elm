module Main exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Browser
import JokesModel exposing (..)
import JokesView exposing (..)
import JokesUpdate exposing (..)
main : Program () Model Msg
main =
    Browser.element 
    { init = \() -> initialModel
    , subscriptions = subscriptions
    , update = update
    , view = view
    }