module JokesModel exposing (..)
import Http

type alias Model =
    { jokes : List String
    , category : String
    , categoryList : List String
    }

type Msg
    = Load
    | NewJoke 
    | ChangeCategory String
    | NewCategoryJoke
    | GotJoke (Result Http.Error String)
    | GotCategories (Result Http.Error String)

initialModel : (Model, Cmd Msg)
initialModel = 
    ({jokes = [], category = "", categoryList = []}, fetchNewCategories)

subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none

fetchNewJoke : Cmd Msg
fetchNewJoke =
  Http.get
    { url = "https://api.chucknorris.io/jokes/random"
    , expect = Http.expectString GotJoke
    }

fetchNewCategoryJoke : Model -> Cmd Msg
fetchNewCategoryJoke model =
  Http.get
    { url = "https://api.chucknorris.io/jokes/random?category=" ++ model.category
    , expect = Http.expectString GotJoke
    }

fetchNewCategories : Cmd Msg
fetchNewCategories =
  Http.get
    { url = "https://api.chucknorris.io/jokes/categories"
    , expect = Http.expectString GotCategories
    }