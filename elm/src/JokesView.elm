module JokesView exposing (..)
import Html exposing (..)
import JokesModel exposing (..)
import Html.Events exposing (onClick)
import JokesUpdate exposing (..)
import JokesModel exposing (..)
import Html.Events exposing (onInput)
import Html.Attributes exposing (placeholder)
import Html.Attributes exposing (value)

view : Model -> Html Msg
view model =
    let
        jokeList = 
            ul [] (List.map (\l -> li [] [ text l ]) model.jokes)
        categoryList = 
            span [] (List.map (\l -> button [onClick (ChangeCategory l)] [text l] ) model.categoryList)
    in
    
    div [] 
    [ h1 [] [text "Jokes App"]
    , h2 [] [text "Categories"]
    , categoryList
    , br [] []
    , br [] []
    , h2 [] [ text "Jokes" ]
    , span [] [
        input [ placeholder "Joke category", value model.category, onInput ChangeCategory ] []
        , button [ onClick NewCategoryJoke ] [ text "New Category Joke" ]
    ]
    , br [] []
    , br [] []
    , button [ onClick NewJoke ] [ text "New Random Joke" ]
    , jokeList
    ]