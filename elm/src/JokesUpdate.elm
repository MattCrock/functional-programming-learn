module JokesUpdate exposing (..)

import JokesModel exposing (..)
import Json.Decode exposing (..)

update : Msg -> Model -> (Model, Cmd Msg)
update msg model = 
    case msg of
        Load ->
            (model, Cmd.none)
        NewJoke ->
            (model, fetchNewJoke)
        GotJoke (Ok joke) -> 
            let
                jokeValue : Result Error String
                jokeValue = decodeString (field "value" string) joke
            in
                case jokeValue of 
                    Err _ ->
                        ({model | jokes = joke :: model.jokes}, Cmd.none)
                    Ok val ->
                        ({model | jokes = val :: model.jokes}, Cmd.none)
        GotJoke (Err error) -> 
            (model, Cmd.none)
        ChangeCategory (category) ->
            ({model | category = category}, Cmd.none)
        NewCategoryJoke ->
            (model, fetchNewCategoryJoke model)
        GotCategories (Ok categories) ->
            let
                categoryValues = decodeString (list string) categories
            in
                case categoryValues of
                    Err _ ->
                        (model, Cmd.none)
                    Ok vals ->
                        ({model | categoryList = vals}, Cmd.none)
        GotCategories (Err error) ->
            (model, Cmd.none)
        -- _ ->
        --     (model, Cmd.none)